package main

import (
	"encoding/json"
	"regexp"
)

type Node struct {
	Prefix string
	Len    int
	Nodes  [128]*Node
}
type Trie struct {
	Nodes [128]*Node
}

/** Initialize your data structure here. */
func Constructor() *Trie {
	return new(Trie)
}

/** Inserts a word into the trie. */
func (this *Trie) Insert(word string) {
	//print(word)
	node := this.Nodes[word[0]]
	wordLen := len(word)
	if node == nil {
		this.Nodes[word[0]] = &Node{word, wordLen, [128]*Node{}}
		//println(" 1")
		return
	}
	bgn := 0
	end := len(node.Prefix)
	for end < wordLen && node.Prefix == word[bgn:end] {
		_node := node.Nodes[word[end]]
		if _node == nil {
			node.Nodes[word[end]] = &Node{word[end:], wordLen, [128]*Node{}}
			//println(" 2")
			return
		}
		node = _node
		bgn = end
		end += len(node.Prefix)
	}
	if end == wordLen && node.Prefix == word[bgn:end] {
		node.Len = wordLen
		//println(" 3")
		return
	}
	i := 0
	for bgn < wordLen && node.Prefix[i] == word[bgn] {
		i++
		bgn++
	}
	nodes := [128]*Node{}
	nodes[node.Prefix[i]] = &Node{node.Prefix[i:], node.Len, node.Nodes}
	if bgn < wordLen {
		nodes[word[bgn]] = &Node{word[bgn:], wordLen, [128]*Node{}}
		//print(" 4")
	}
	node.Len = wordLen
	node.Prefix = node.Prefix[:i]
	node.Nodes = nodes
	//println(" 5")
	return
}

/** Returns if the word is in the trie. */
func (this *Trie) Search(word string) bool {
	wordLen := len(word)
	bgn, end := 0, 0
	node := this.Nodes[word[bgn]]
	for node != nil {
		end += len(node.Prefix)
		if end <= wordLen && node.Prefix == word[bgn:end] {
			if end == wordLen {
				return node.Len == wordLen
			}
			bgn = end
			node = node.Nodes[word[bgn]]
		} else {
			return false
		}
	}
	return false
}

/** Returns if there is any word in the trie that starts with the given prefix. */
func (this *Trie) StartsWith(prefix string) bool {
	prfxLen := len(prefix)
	bgn, end := 0, 0
	node := this.Nodes[prefix[bgn]]
	for node != nil {
		_end := len(node.Prefix)
		if prfxLen <= (end + _end) {
			return node.Prefix[:prfxLen-bgn] == prefix[bgn:]
		} else {
			end += _end
			if node.Prefix != prefix[bgn:end] {
				return false
			}
			bgn = end
			node = node.Nodes[prefix[bgn]]
		}
	}
	return false
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
func main() {
	type Tests []struct {
		Op, Arg string
	}
	tests := Tests{
		{"insert", "abc"},
		{"insert", "bcd"},
		{"insert", "acde"},
		{"insert", "acd"},
		{"insert", "bcdef"},
		{"insert", "acefg"},
		{"insert", "acegh"},
		{"insert", "ace"},
		{"search", "abc"},
		{"search", "bcd"},
		{"search", "acde"},
		{"search", "acd"},
		{"search", "bcdef"},
		{"search", "acefg"},
		{"search", "acegh"},
		{"search", "ace"},
		{"startsWith", "ab"},
		{"startsWith", "bc"},
		{"startsWith", "acd"},
		{"startsWith", "ac"},
		{"startsWith", "bcde"},
		{"startsWith", "acef"},
		{"startsWith", "aceg"},
	}
	bs, _ := json.MarshalIndent(tests, "", "  ")
	println(string(bs))
	var rslt []bool
	trie := Constructor()
	for _, test_ := range tests {
		switch test_.Op {
		case "insert":
			trie.Insert(test_.Arg)
		case "search":
			rslt = append(rslt, trie.Search(test_.Arg))
		case "startsWith":
			rslt = append(rslt, trie.StartsWith(test_.Arg))
		}
	}
	bs, _ = json.Marshal(rslt)
	println("result:", string(bs))
	bs, _ = json.MarshalIndent(trie.Nodes, "", "  ")
	re, _ := regexp.Compile("\n +null,*")
	bs = re.ReplaceAll(bs, []byte(""))
	re, _ = regexp.Compile(",\n +\"Nodes\": \\[\n +\\]")
	println(string(re.ReplaceAll(bs, []byte(""))))
}
